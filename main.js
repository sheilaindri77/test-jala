$(function () {
  $(document).scroll(function () {
    var $nav = $(".fixed-top");
    $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
  });
});

$(function() {
  $('#inputOffice, #inputReferral').on('input', function() {
    $('#inputNotes').val(
      $('#inputOffice, #inputReferral').map(function() {
        return $(this).val();
      }).get().join(';;;') /* added space */
    );

    var notes = $('#inputNotes').val()
    console.log(notes);
    var split = notes.split(';;;')
    if(split.length>1){
      var perusahaan = 'Perusahaan : ' + split[0]
      var referral = 'Referral : ' + split[1]
      $('#inputNotes').val(perusahaan + '\n' + referral)
    }
    console.log($('#inputNotes').val());
  });
});
