FROM nginx:stable-alpine
LABEL version="1.0"

COPY app/nginx.conf.template /etc/nginx/conf.d/default.conf.template

COPY app/docker-entrypoint.sh /

WORKDIR /app
COPY . ./referral

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]
