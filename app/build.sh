#!/bin/bash
set -e

# kadang harus sudo su, https://github.com/docker/buildx/issues/415#issuecomment-761857828

DOCKERTAG=latest

if [ "$1" = "dev" ]; then
  DOCKERTAG=dev
fi

docker build -t jala/v2/web/referral:$DOCKERTAG .
