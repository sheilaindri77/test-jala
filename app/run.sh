#!/bin/bash

DOCKERTAG=latest
if [ "$1" = "dev" ]; then
  DOCKERTAG=dev
fi

docker stop web-referral

docker run --rm -p 8110:80 \
  --name web-referral \
  -e "JALA_SCRIPT=https://lontong.com" \
  jala/v2/web/referral:$DOCKERTAG