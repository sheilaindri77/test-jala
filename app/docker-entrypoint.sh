#!/usr/bin/env sh
set -eu

envsubst '${JALA_SCRIPT} ${JALA_CHANNEL_CODE}' < /etc/nginx/conf.d/default.conf.template > /etc/nginx/conf.d/default.conf

exec "$@"